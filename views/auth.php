<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>TL SOLUTIONS</title>
        <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-4.0.0-dist/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
    </head>    
    <body class="d-flex justify-content-center align-items-center">
     <?php
     include 'login.php';         
     ?>
        <script type="text/javascript" src="assets/plugins/jquery/jquery.js"></script>
    </body> 
</html>


